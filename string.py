
print("hola, si te gusta programar y estas dispuesto a aprender acerca de las cadenas de texto este es el lugar indicado.")
print(" ¿sabes que es una cadena de texto?")
cadena=int(input("ingrese el numero # 1 si quiere saber que es:"))
if cadena==1:
    print("Es un conjunto de caracteres alfanumericos que constituyen una palabra, pueden ser vistos como una tupla de caracteres")
    

print("Ahora veremos los metodos funcionales de los string")


Ejemplo1=int(input("funcion .upper() ingrese 1 para continuar" ))
if Ejemplo1==1:
    print("el metodo .upper, convertira su cadena de texto en una cadena en mayusculas")
    Ejemplo1=input("ingrese una cadena de texto, ej:El nombre de su mascota y una cualidad:")
a= Ejemplo1.upper()
print(a)
Ejemplo1=int(input("funcion .lower() ingrese 1 para continuar" ))
if Ejemplo1==1:
    print("si desea que la cadena de texto guardada en la variable a se conviertsa nuevamente en minuscula puede usar la funcion .lower")
print(a.lower())

Ejemplo2=int(input("funcion .strip() ingrese 1 para continuar:" ))
if Ejemplo2==1:
    print("si desea quitar espacios en blanco innecesarios al inicio y final de la cadena utilice la funcion .strip()")
Ejemplo2=("  REG 123 678 345   ")
b=Ejemplo2.strip()
print(b)
Ejemplo2=int(input("funcion .startswith() ingrese 1 para continuar:" ))
if Ejemplo2==1:
    print("si desea saber si la cadena inicia como un registro puede utilizar .startswith(REG)")
print(b.startswith("REG"))

print("puede utilizar todas las funciones en una sola")
print(b.lower(),b.strip(),b.startswith("reg"))

Ejemplo2=int(input("funcion .endswith() ingrese 1 para continuar:" ))
if Ejemplo2==1:
    print("si no le interesa las iniciales del registro sino los ultimos digitos utiliza la funcion .endwith("")")
print(b.endswith("45"))

Ejemplo2=int(input("funcion .find() ingrese 1 para continuar:" ))
if Ejemplo2==1:
    print("buscar algo en la cadena de texto,ej: grupo en el que se encuentra")
print(b.find("67")),print(("este imprime la posicion en la cadena que se encuentra el primer valor de la busqueda"))

#los primeros digitos indican la ubicacion del objeto o animal, si el animal se cambia de ubicacion se puede reemplazar en los primeros digitos despues del reg
Ejemplo2=int(input("funcion .replace() ingrese 1 para continuar:" ))
if Ejemplo2==1:
    print("Reemplazar valores en el registro ej: 123 por 121")
print(b.replace("123","121"))
print("si se quiere separar la ubicacion,grupo,numero de animal, usar .split()" )

Ejemplo2=int(input("funcion .split() ingrese 1 para continuar:" ))
if Ejemplo2==1:
    print(b.split(" "))
    
print("como ultimo concepto cabe resaltar que las cadenas no se pueden modificar son inmutables")
print("si quiere saber si la cadena es un registro y la ubicacion del animal puede imprimir la posicion 0 hasta la posicion del ultimo digito de ubicacion")
print(b[0:8])

print("felicitaciones ya tienes mas conocimiento acerca de las cadenas de texto y sus caracteristicas")
